# Booker
This is a simple bookkeeping application using [Express framework](https://expressjs.com/).

### Tech Stack
  * Node.js / Express 4
  * Mongo
  * Docker
  * Pug
  * Tailwind CSS
  * Webpack
  * Redis
  * PM2

### Installation
**Requirements**
  * [Node.js 10+](https://nodejs.org/en/)
  * [Yarn](https://yarnpkg.com/lang/en/)
  * [Docker](https://www.docker.com/)

**Running locally:**
```sh
git clone git@gitlab.com:zhadaev/pet-nodejs.git
yarn
cp .env.example .env
```
Run redis and mongo in containers:
```sh
docker-compose up --build
```
Run application and serve static:
```sh
yarn dev
```
Then visit [http://localhost:3002/](http://localhost:3002/)


### License
MIT