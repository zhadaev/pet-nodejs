const request = async (method, url, data) => {
  const response = await fetch(url, {
    method,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    // mode: 'no-cors',
    // cache: 'no-cache',
    // credentials: 'same-origin',
    // redirect: 'follow',
    // referrer: 'no-referrer',
    body: JSON.stringify(data)
  })

  return response.json()
}

const post = (url, data) => {
  return request('POST', url, data)
}

const get = (url, data) => {
  return request('GET', url, data)
}
