(() => {
  const registerForm = document.querySelector('#registerForm');
  registerForm.addEventListener('submit', async (event) => {
    event.preventDefault()
    
    const login = document.querySelector('#login')
    const password = document.querySelector('#password')
    const passwordConfirm = document.querySelector('#passwordConfirm')
    const data = {login: login.value, password: password.value, passwordConfirm: passwordConfirm.value}
    
    try {
      const response = await post('/api/user/register', data)
      
      if(!response.ok) {
        throw new Error(response.message)
      }
      iziToast.success({
        title: '',
        message: 'Your account was successfully created.<br> Redirecting in 3 seconds...',
        position: 'topRight'
      })
      setTimeout(() => window.location = '/login', 3000)
    } catch(err) {
      iziToast.error({
        title: 'Error',
        message: err.message,
        position: 'topRight'
      })
    }
  })
})()