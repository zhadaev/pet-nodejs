import Chart from 'chart.js'

;(() => {
  const purchases = document.querySelector('#purchases')
  if (purchases) {
    const data = JSON.parse(purchases.dataset.purchases)
    new Chart(purchases, {
      type: 'doughnut',
      data,
      options: {
        responsive: true,
        legend: {
          display: true,
          labels: {
            paddingBottom: 50
          }
        }
      }
    })
  }
})()
