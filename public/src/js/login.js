;(() => {
  const loginForm = document.querySelector('#loginForm')
  loginForm.addEventListener('submit', async event => {
    event.preventDefault()

    const login = document.querySelector('#login')
    const password = document.querySelector('#password')
    const data = { login: login.value, password: password.value }

    try {
      const response = await post('/api/user/login', data)

      if (!response.ok) {
        throw new Error(response.message)
      }
      window.location = '/'
    } catch (err) {
      iziToast.error({
        title: 'Error',
        message: err.message,
        position: 'topRight'
      })
    }
  })
})()
