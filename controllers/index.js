const Category = require('./category')
const Purchase = require('./purchase')
const Dashboard = require('./dashboard')
const User = require('./user')
const Reports = require('./reports')

module.exports = {
  Category,
  Purchase,
  Dashboard,
  User,
  Reports
}
