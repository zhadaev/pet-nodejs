const Models = require('../models')
const helpers = require('../shared/helpers')
const asyncHandler = require('../shared/middleware/async')
const CONFIG = require('../shared/config')

exports.getPurchases = async (req, res) => {
  const { userId } = req.session
  const { page = 1 } = req.params
  const perPage = CONFIG.ITEMS_PER_PAGE
  const purchasesCount = await Models.UserPurchase(userId).countDocuments()

  const purchases = await Models.UserPurchase(userId)
    .skip(perPage * page - perPage)
    .limit(perPage)
    .sort('-createdAt')
    .populate({
      path: 'category',
      populate: {
        path: 'parent',
        model: 'Category'
      }
    })

  if (page > 1 && purchases.length === 0) {
    res.redirect('/purchases')
  }
  const categories = await Models.UserCategory(userId)

  res.render('pages/purchases/purchases', {
    purchases: helpers.purchaseCategoriesParser(purchases),
    categories,
    user: req.session.userLogin,
    currentPage: Number(page),
    pages: Math.ceil(purchasesCount / perPage)
  })
}

exports.createPurchase = async (req, res) => {
  const { userId } = req.session
  const categories = await Models.UserCategory(userId)

  try {
    const { amount, notes, category } = req.body

    if (amount.trim().length === 0) {
      throw new Error('You have to enter amount value')
    }
    if (category === '') {
      throw new Error('You have to choose category')
    }

    await Models.Purchase.create({
      amount,
      notes,
      category,
      user: userId
    })
    res.redirect('/purchases')
  } catch (error) {
    res.render('pages/purchases/add', {
      error,
      categories,
      user: req.session.userLogin
    })
  }
}

exports.getPurchase = async (req, res) => {
  try {
    const { id } = req.params
    const { userId } = req.session
    const purchase = await Models.Purchase.findById(id)
    const categories = await Models.UserCategory(userId)

    res.render('pages/purchases/purchase', {
      purchase,
      categories,
      user: req.session.userLogin
    })
  } catch (_) {
    res.render('pages/404')
  }
}

exports.updatePurchase = async (req, res) => {
  const { id } = req.params
  const { amount, notes, category, remove } = req.body
  const { userId } = req.session
  const purchase = await Models.Purchase.findById(id)
  const categories = await Models.UserCategory(userId)

  if (remove) {
    await Models.UserPurchase(userId).findOneAndDelete({ _id: id })
    res.redirect('/purchases')
  }
  if (amount.trim().length === 0) {
    res.render('pages/purchases/purchases', {
      error: 'You have to enter amount value',
      user: req.session.userLogin,
      categories,
      purchase
    })
    return
  }
  if (category === '') {
    res.render('pages/purchases/purchase', {
      error: 'You have to choose category',
      user: req.session.userLogin,
      categories,
      purchase
    })
    return
  }

  await Models.Purchase.findByIdAndUpdate(id, {
    amount,
    notes,
    category,
    user: req.session.userId
  })
  res.redirect('/purchases')
}

exports.getAddPurchasePage = asyncHandler(async (req, res) => {
  const { userId } = req.session
  const categories = await Models.UserCategory(userId)

  res.render('pages/purchases/add', {
    categories,
    user: req.session.userLogin
  })
})
