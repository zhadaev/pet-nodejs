const Models = require('../models')
const helpers = require('../shared/helpers')

exports.getAddCategoryPage = async (req, res) => {
  const { userId } = req.session
  const categories = await Models.UserCategory(userId)
  res.render('pages/categories/add', {
    categories: helpers.categoriesParser(categories),
    user: req.session.userLogin
  })
}

exports.getCategories = async (req, res) => {
  const { userId } = req.session
  const categories = await Models.UserCategory(userId)
  res.render('pages/categories/categories', {
    categories: helpers.categoriesParser(categories),
    user: req.session.userLogin
  })
}

exports.createCategory = async (req, res) => {
  const { title, notes, parent } = req.body
  const { userId } = req.session
  const categories = await Models.UserCategory(userId)

  if (title.trim().length === 0) {
    res.render('pages/categories/categories', {
      error: 'You have to enter category name',
      categories: helpers.categoriesParser(categories)
    })
    return
  }
  try {
    const category = await Models.UserCategory(userId).findOne({ title })
    const user = req.session.userId

    if (!category || category.parent !== parent) {
      await Models.Category.create({
        title,
        notes,
        parent,
        user
      })
      res.redirect('/categories')
    } else {
      res.render('pages/categories/categories', {
        error: 'Such category already exists',
        categories: helpers.categoriesParser(categories)
      })
    }
  } catch (err) {
    console.log(err)
  }
}

exports.getCategory = async (req, res) => {
  try {
    const { userId } = req.session
    const categories = await Models.UserCategory(userId)
    const category = await Models.Category.findById(req.params.id)

    if (!category) {
      throw new Error('There is no category with such id')
    }

    res.render('pages/categories/category', {
      user: req.session.userLogin,
      category,
      categories
    })
  } catch (error) {
    res.render('pages/404')
  }
}

exports.updateCategory = async (req, res) => {
  const { id } = req.params
  const { title, notes, parent, remove } = req.body
  const { userId } = req.session
  if (remove) {
    await Models.UserCategory(userId).findOneAndDelete({ _id: id })
    res.redirect('/categories')
  }
  try {
    await Models.UserCategory(userId).findOneAndUpdate(id, {
      title,
      notes,
      parent
    })
    res.redirect('/categories')
  } catch (e) {
    console.log(e)
  }
}
