const Models = require('../models')
const moment = require('moment')

exports.getDashboard = async (req, res) => {
  const { userId } = req.session
  const lastPurchases = await Models.UserPurchase(userId).getLastPurchases()
  const topCategories = await Models.Purchase.getCategoriesWithMaxPurchases(userId)

  res.render('pages/dashboard', {
    purchases: lastPurchases,
    topCategories,
    user: req.session.userLogin,
    formatDate: date => moment(date).format('DD.MM.YYYY')
  })
}