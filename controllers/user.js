const Models = require('../models')
const bcrypt = require('bcrypt')
const CONFIG = require('../shared/config')

exports.getRegisterPage = (req, res) => res.render('pages/register')

exports.registerUser = async (req, res) => {
  const { login, password, passwordConfirm } = req.body

  if (!(login && password && passwordConfirm)) {
    res.render('pages/register', { error: 'You have to fill all fields' })
  } else if (password !== passwordConfirm) {
    res.render('pages/register', { error: "Passwords don't match" })
  } else if (password.length < 5) {
    res.render('pages/register', {
      error: "Password's length should be greater than 5"
    })
  } else if (login.length < 5) {
    res.render('pages/register', {
      error: "Login's length should be greater than 5"
    })
  } else {
    const user = await Models.User.findOne({ login })

    if (!user) {
      const salt = bcrypt.genSaltSync(Number(CONFIG.SALT_ROUNDS))

      Models.User.create({
        login,
        password: bcrypt.hashSync(password, salt),
        is_new: true
      })
      res.redirect('/login')
    } else {
      res.render('pages/register', {
        error: 'User with such login already exists'
      })
    }
  }
}

exports.getLoginPage = (req, res) => {
  if (req.session.userId) {
    res.redirect('/')
  }
  res.render('pages/login')
}

exports.loginUser = async (req, res) => {
  const { login, password } = req.body
  if (!(login && password)) {
    res.render('pages/login', {
      error: 'You have to enter login and password'
    })
  } else {
    try {
      const user = await Models.User.findOne({ login })

      if (!user) {
        return res.render('pages/login', {
          error: 'There is no user with such login and password'
        })
      }
      bcrypt.compare(password, user.password, function(err, result) {
        if (!result) {
          return res.render('pages/login', {
            error: 'There is no user with such login and password'
          })
        }
        req.session.userId = user.id
        req.session.userLogin = user.login
        res.redirect('/')
      })
    } catch (err) {
      console.log(err)
    }
  }
}

exports.logoutUser = (req, res) => {
  if (req.session) {
    req.session.destroy(() => {
      res.redirect('/login')
    })
  }
}