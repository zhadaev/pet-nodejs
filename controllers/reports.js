const Models = require('../models')
const helpers = require('../shared/helpers')

exports.getReportsPage = async (req, res) => {
  const { userId } = req.session
  const _purchases = await Models.Purchase.getCategoriesAmount(userId)
  const purchases =
    _purchases.length > 0
      ? {
          datasets: [
            {
              data: _purchases.map(item => item.amount),
              backgroundColor: Array.from({
                length: _purchases.length
              }).map(_ => helpers.randomColorGenerator())
            }
          ],
          labels: _purchases.map(item => item.category)
        }
      : null

  res.render('pages/reports', { purchases, user: req.session.userLogin })
}