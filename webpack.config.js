const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const FileManagerPlugin = require('filemanager-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
  entry: {
    reports: path.join(__dirname, 'public', 'src', 'js', 'reports.js'),
    styles: ['./public/src/styles/index.css']
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'public', 'dist')
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin({extractComments: true}), new OptimizeCSSAssetsPlugin({})]
  },
  mode: process.env.NODE_ENV || 'development',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader'
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: 'output.css',
    }),
    new FileManagerPlugin({
      onEnd: {
        delete: [
          './public/dist/styles.js',
          './public/dist/*.js.LICENSE'
        ]
      }
    }),
    new WebpackNotifierPlugin({
      title: 'Webpack',
      alwaysNotify: true
    })
  ],
  watchOptions: {
    ignored: /node_modules/,
    aggregateTimeout: 300,
    poll: 500
  }
}
