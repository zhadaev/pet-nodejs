FROM node:10

RUN yarn global add pm2 webpack webpack-cli

WORKDIR /usr/src/app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --silent

COPY . .

EXPOSE $APP_PORT

# CMD ["pm2-runtime", "pm2-apps.yml"]

CMD ["yarn", "dev"]
