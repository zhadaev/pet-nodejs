const app = require('./app')
const CONFIG = require('./shared/config')
const DBConnect = require('./shared/database')
const colors = require('colors')

DBConnect() 

const server = app.listen(CONFIG.APP_PORT, () =>
  console.log(`Pet app has been started on http://${CONFIG.HOST}:${CONFIG.APP_PORT}!`.yellow.bold)
)

process.on('unhandledRejection', (err) => {
  console.log(`Unhandled Rejection: ${err.message}`.red)
  server.close(() => process.exit(1))
})
