const categoriesParser = require('./categories-parser')
const purchaseCategoriesParser = require('./purchase-category-parser')
const randomColorGenerator = require('./random-color-generator')

module.exports = {
  categoriesParser,
  purchaseCategoriesParser,
  randomColorGenerator
}
