module.exports = categories => {
  return categories
    .filter(category => !category.parent)
    .map(rootCategory => ({
      ...rootCategory.toObject(),
      subcategories: categories.filter(
        cat => cat.parent && cat.parent.id === rootCategory.id
      )
    }))
}
