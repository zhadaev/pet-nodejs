const moment = require('moment')

module.exports = (purchases, dashboard = false) => {
  if (purchases.length === 0) {
    return {}
  }
  return purchases
    .map(
      ({
        amount,
        notes,
        _id,
        createdAt,
        category: { title: category, parent }
      }) => ({
        amount,
        id: _id,
        notes,
        createdAt: moment(createdAt).format('DD.MM.YYYY'),
        category,
        parent: parent ? parent.category : null
      })
    )
    .reduce((acc, curr) => {
      acc.hasOwnProperty(curr.createdAt)
        ? acc[curr.createdAt].push(curr)
        : (acc[curr.createdAt] = [curr])

      return acc
    }, {})
}
