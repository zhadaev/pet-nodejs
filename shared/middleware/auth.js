const Models = require('../../models')

module.exports.isAuth = async (req, res, next) => {
  try {
    const userId = req.session.userId
    const user = await Models.User.findById(userId)
    if(!user) {
      res.redirect('/login')
    } else {
      next()
    }
  } catch(err) {
    next(err)
  }
} 