const CONFIG = require('../config')

const errorHandler = (err, req, res, next) => {
  let message = err.message
  
  if (err.name === 'ValidationError') {
    message = Object.values(err.errors).map(val => val.message)
    next()
  }
  if (err.name === 'CastError') {
    message = 'Resource not found'
  }
}

module.exports = errorHandler;