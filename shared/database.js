const mongoose = require('mongoose')
const CONFIG = require('./config')

const DBConnect = async () => {
  const connect = await mongoose.connect(CONFIG.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  })
  console.log(`Mongo host: ${connect.connection.host}`.green.bold)
}

module.exports = DBConnect
