const dotenv = require('dotenv')
const path = require('path')

dotenv.config({ path: path.join(__dirname, '..', '.env') })

module.exports = {
  HOST: process.env.HOST || 'localhost',
  APP_PORT: process.env.APP_PORT || 3001,
  SALT_ROUNDS: process.env.SALT_ROUNDS,
  SESSION_SECRET: process.env.SESSION_SECRET,
  MONGO_URL: process.env.MONGO_URL || `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`,
  REDIS_PORT: process.env.REDIS_PORT,
  REDIS_HOST: process.env.REDIS_HOST,
  VIEWS_FOLDER: path.join(__dirname, '..', 'views'),
  ITEMS_PER_PAGE: 10
}
