const Model = require('../models')
const authMiddleware = require('../shared/middleware/auth')
const Controllers = require('../controllers')

module.exports = app => {
  app.get('/reports', authMiddleware.isAuth, Controllers.Reports.getReportsPage)
}
