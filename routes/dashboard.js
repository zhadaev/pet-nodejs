const authMiddleware = require('../shared/middleware/auth')
const Controllers = require('../controllers')

module.exports = app => {
  app.get('/', authMiddleware.isAuth, Controllers.Dashboard.getDashboard)
}
