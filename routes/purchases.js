const authMiddleware = require('../shared/middleware/auth')
const Controllers = require('../controllers')

module.exports = app => {
  app.get(
    '/purchase',
    authMiddleware.isAuth,
    Controllers.Purchase.getAddPurchasePage
  )
  app.post(
    '/purchase',
    authMiddleware.isAuth,
    Controllers.Purchase.createPurchase
  )
  app.get(
    '/purchase/:id',
    authMiddleware.isAuth,
    Controllers.Purchase.getPurchase
  )
  app.post(
    '/purchase/:id',
    authMiddleware.isAuth,
    Controllers.Purchase.updatePurchase
  )
  app.get(
    '/purchases/:page*?',
    authMiddleware.isAuth,
    Controllers.Purchase.getPurchases
  )
}
