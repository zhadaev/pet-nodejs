const authMiddleware = require('../shared/middleware/auth')

module.exports = app => {
  app.get('/initial_settings', authMiddleware.isAuth, (req, res) => {
    res.render('pages/initial_settings')
  })
}
