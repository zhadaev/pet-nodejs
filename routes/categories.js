const authMiddleware = require('../shared/middleware/auth')
const Controllers = require('../controllers')

module.exports = app => {
  app.get(
    '/category',
    authMiddleware.isAuth,
    Controllers.Category.getAddCategoryPage
  )
  app.post(
    '/category',
    authMiddleware.isAuth,
    Controllers.Category.createCategory
  )
  app.get(
    '/category/:id',
    authMiddleware.isAuth,
    Controllers.Category.getCategory
  )
  app.post(
    '/category/:id',
    authMiddleware.isAuth,
    Controllers.Category.updateCategory
  )
  app.get(
    '/categories',
    authMiddleware.isAuth,
    Controllers.Category.getCategories
  )
}
