const Controllers = require('../controllers')

module.exports = app => {
  app.get('/register', Controllers.User.getRegisterPage)

  app.post('/register', Controllers.User.registerUser)
  
  app.get('/login', Controllers.User.getLoginPage)

  app.post('/login', Controllers.User.loginUser)

  app.get('/logout', Controllers.User.logoutUser)
}
