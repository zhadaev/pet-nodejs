const user = require('./user')
const dashboard = require('./dashboard')
const purchases = require('./purchases')
const categories = require('./categories')
const reports = require('./reports')
const settings = require('./settings')

const registerRoutes = (app) => {
  dashboard(app)
  user(app)
  categories(app)
  purchases(app)
  reports(app),
  settings(app)
}

module.exports = registerRoutes
