const mongoose = require('mongoose')
const schema = mongoose.Schema

const currency = new schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    name_plural: {
        type: String,
        required: true,
        trim: true,
        },
    symbol: {
      type: String,
      required: false,
      trim: true,
    },
    symbol_native: {
      type: String,
      required: false,
      trim: true
    },
    code: {
      type: String,
      required: true,
      trim: true
    },
  },
  {
    timestamps: true,
  }
)

category.set('toJSON', {
  getters: true,
  virtuals: true,
})

module.exports = mongoose.model('Currency', currency)
