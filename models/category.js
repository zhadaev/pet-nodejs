const mongoose = require('mongoose')
const schema = mongoose.Schema

const setParent = parent => {
  return !parent || parent === '' ? null : parent
}

const category = new schema(
  {
    title: {
      type: String,
      required: [true, 'Please add category title'],
      trim: true
    },
    notes: {
      type: String,
      required: false,
      trim: true
    },
    parent: {
      type: schema.Types.ObjectId,
      required: false,
      ref: 'Category',
      set: setParent
    },
    user: {
      type: schema.Types.ObjectId,
      required: true,
      ref: 'User'
    }
  },
  {
    timestamps: true
  }
)

category.set('toJSON', {
  getters: true,
  virtuals: true
})

module.exports = mongoose.model('Category', category)
