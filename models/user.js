const mongoose = require('mongoose')
const schema = mongoose.Schema

const user = new schema(
  {
    login: {
      type: String,
      required: true,
      unique: true,
      trim: true
    },
    password: {
      type: String,
      required: true,
      trim: true
    },
    is_new: {
      type: Boolean,
      required: false,
      default: true
    }
  },
  {
    timestamps: true,
  }
)

user.set('toJSON', {
  getters: true,
  virtuals: true,
})

module.exports = mongoose.model('User', user)
