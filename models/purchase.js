const mongoose = require('mongoose')
const schema = mongoose.Schema

const purchase = new schema(
  {
    amount: {
      type: Number,
      required: [true, 'Please add purchase amount'],
      trim: true
    },
    category: {
      type: schema.Types.ObjectId,
      required: [true, 'Please select category'],
      ref: 'Category'
    },
    notes: {
      type: String,
      required: false,
      trim: true
    },
    user: {
      type: schema.Types.ObjectId,
      required: true,
      ref: 'User'
    }
  },
  {
    timestamps: true
  }
)

purchase.query.getLastPurchases = async function(count = 10) {
  return await this.sort('-createdAt')
    .populate({
      path: 'category',
      populate: {
        path: 'parent',
        model: 'Category'
      }
    })
    .limit(count)
}

purchase.statics.getCategoriesWithMaxPurchases = async function(userId) {
  return await this.aggregate([
    {
      $match: { user: mongoose.Types.ObjectId(userId) }
    },
    {
      $group: {
        _id: '$category',
        count: { $sum: 1 }
      }
    },
    {
      $lookup: {
        from: 'categories',
        localField: '_id',
        foreignField: '_id',
        as: 'categoryData'
      }
    },
    { $unwind: '$categoryData' },
    {
      $addFields: {
        category: '$categoryData.title',
        parentCategoryId: '$categoryData.parent'
      }
    },
    {
      $project: { count: 1, category: 1 }
    },
    { $sort: { count: -1 } }
  ])
}

purchase.statics.getCategoriesAmount = async function(userId) {
  return await this.aggregate([
    {
      $match: { user: mongoose.Types.ObjectId(userId) }
    },
    {
      $group: {
        _id: '$category',
        amount: { $sum: '$amount' }
      }
    },
    {
      $lookup: {
        from: 'categories',
        localField: '_id',
        foreignField: '_id',
        as: 'categoryData'
      }
    },
    {
      $unwind: '$categoryData'
    },
    {
      $addFields: {
        category: '$categoryData.title'
      }
    },
    {
      $project: { amount: 1, category: 1 }
    }
  ])
}

purchase.set('toJSON', {
  getters: true,
  virtuals: true
})

module.exports = mongoose.model('Purchase', purchase)
