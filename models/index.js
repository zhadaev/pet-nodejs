const User = require('./user')
const Category = require('./category')
const Purchase = require('./purchase')

module.exports = {
    User,
    Category,
    Purchase,
    UserPurchase: (user) => Purchase.where({user}),
    UserCategory: (user) => Category.where({user}).populate('parent')
}