const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()
const registerRoutes = require('./routes')
const CONFIG = require('./shared/config')
const colors = require('colors')

const session = require('express-session')
const redis = require('redis')
const redisClient = redis.createClient(CONFIG.REDIS_PORT, CONFIG.REDIS_HOST)
const redisStore = require('connect-redis')(session)

redisClient.on('error', err => {
  console.log(`Redis error: ${err}`.red)
})

app.use(
  session({
    secret: CONFIG.SESSION_SECRET,
    resave: false,
    name: '_sessionID',
    saveUninitialized: false,
    store: new redisStore({
      client: redisClient,
      ttl: 86400
    }),
    cookie: {
      maxAge: 60 * 60 * 1000 * 2 // 2 hours
    }
  })
)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')))

app.set('views', CONFIG.VIEWS_FOLDER)
app.set('view engine', 'pug')

registerRoutes(app) 

app.use((_, res) => {
  res.status(404).render('pages/404')
})

module.exports = app
